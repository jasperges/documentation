.. _web_remote:

The Web Remote
==============

OpenLP gives you the ability to control the :ref:`creating_service`. You also 
have the ability to search for and add or display media from the 
:ref:`media-manager` or send an :ref:`alerts`, using a remote computer, netbook 
or smartphone and a web browser. You can use the web remote to control the 
entire service away from the main computer or, have the nursery or daycare send 
an :ref:`alerts` message to the projection screen. This could also be used for a 
guest speaker or worship team leader to control their own slides if needed.

The first step is to make sure the Remote plugin is activated. You can accomplish
this by following the instructions in the :ref:`plugin_list`. The second step is 
to configure the settings you will use with the web browser. You can find these 
instructions and settings in :ref:`remote_tab`.

Open a web browser, type in the Remote URL you found in :ref:`remote_tab` and 
press the :kbd:`Enter` key. For this example we will use
\http://192.168.1.73:4316. You will then be presented with the home page of the
OpenLP Remote.

.. image:: pics/web_remote_start.png

Service Manager
---------------

Clicking on :guilabel:`Service Manager` will display the service item list from 
the :ref:`creating_service`.

.. image:: pics/web_remote_service.png

Clicking on any item in the Service Manager will immediately take you to the 
Slide Controller. 

You will find the following buttons throughout the web remote interface. 

    |web_remote_home| The home button is found on most of the pages in the top 
    left hand corner of the remote interface and clicking it will take you back 
    to the home page of the OpenLP remote.

    |web_remote_refresh| When changes are made on the main computer clicking on 
    refresh will show the changes in the remote viewer.

    |web_remote_blank| Clicking this button will blank the display on the 
    projector screen. Clicking on an item in the Service Manager will 
    automatically show the item on the screen.

    |web_remote_theme| Clicking this button will blank the display on the 
    projector screen to the current theme. Note that this is not available for
    item which does not use themes, such as images, presentations and media. 
    Clicking on an item in the Service Manager will automatically show the 
    item on the screen.

    |web_remote_desktop| Clicking this button will hide the display on the 
    projector screen and therefore show the desktop. Clicking on an item in the
    Service Manager will automatically show the item on the screen.

    |web_remote_show| If the screen is blanked, clicking on this button will
    unblank the display.

    |web_remote_previous| This button will move you upward through the Service 
    Manager and Slide Controller.

    |web_remote_next| This button will move you downward through the Service 
    Manager and Slide Controller.

**Note:** The buttons Blank, Show, Prev and Next are conveniently located on 
the top and the bottom of the remote interface to help eliminate scrolling.

Slide Controller
----------------

After you click on an item in the Service Manager you will be taken to the Slide 
Controller interface. 

.. image:: pics/web_remote_slide1.png

You will find the verses displayed as they appear in the live view on the main 
computer and the first verse will be displayed on the projection screen. You can 
use the :guilabel:`Prev`, :guilabel:`Next` or click on each verse using the 
mouse to display them. 

.. image:: pics/web_remote_slide2.png

When the slides shown are either images or presentations a small thumbnail will
be shown.

**Note:** The remote interface replicates the OpenLP software. When displaying 
images, clicking on a single image in the service manager will display it 
immediately. The first image in a group of images and the first slide in a 
presentation will also be displayed immediately when clicked on in the 
web remote service manager.

Alerts
------

You can send an alert to the projection screen by entering the text in the box 
and clicking on :guilabel:`Show Alert`. The alert will be displayed as you have 
it configured in :ref:`configure_alerts`.

.. image:: pics/web_remote_alert.png

Search
------

Using the search function gives you the ability to search for a particular media, 
add it to the service or display it immediately.

.. image:: pics/web_remote_search.png

Clicking on :guilabel:`Songs` will display your media options to search through. 
Click on the media type that you want to search. 

.. image:: pics/web_remote_search_choice.png

Enter the text you want to search for. You can click :guilabel:`Search` or press 
:kbd:`Enter` on the keyboard. If you do not enter any text to search for, you 
will be presented with your entire list of media for the selected media. 

**Note:** When searching :guilabel:`Bibles` media you must enter the exact name, 
chapter, verse or verses. Searching :guilabel:`Songs` will search titles and 
lyrics with the text used.

For this example we are searching "Songs" and the word "God". The results will 
be displayed below the search button.

.. image:: pics/web_remote_search_complete.png

After finding the song you searched for, clicking on the song will bring up the 
next screen.

.. image:: pics/web_remote_search_options.png

Click on :guilabel:`Go Live` to immediately display your media on the projection 
screen. Click on :guilabel:`Add to Service` to add your media to the bottom of 
the :ref:`creating_service`. Click on :guilabel:`Add & Go to Service` to add
your media to the bottom of the :ref:`creating_service`, and go to the web
remote service manager.

.. These are all the image templates that are used in this page.

.. |WEB_REMOTE_HOME| image:: pics/web_remote_home.png

.. |WEB_REMOTE_REFRESH| image:: pics/web_remote_refresh.png

.. |WEB_REMOTE_BLANK| image:: pics/web_remote_blank.png

.. |WEB_REMOTE_THEME| image:: pics/web_remote_theme.png

.. |WEB_REMOTE_DESKTOP| image:: pics/web_remote_desktop.png

.. |WEB_REMOTE_SHOW| image:: pics/web_remote_show.png

.. |WEB_REMOTE_PREVIOUS| image:: pics/web_remote_previous.png

.. |WEB_REMOTE_NEXT| image:: pics/web_remote_next.png
